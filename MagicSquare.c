
#include <stdio.h>

#define size 3


int magic (int arr[size][size]) 
{
	int i;
	int magic =0 ;
	for (i = 0; i < size; ++i)
	{
		magic +=arr[0][i];
	}
	return magic ;
}


int Is_Magic_Square_Row(int arr[size][size],int _magic)
{
	int sum = 0 ;
	int x,y;
	for (y = 0; y < size; ++y)
	{
		sum =0;
		for (x = 0; x < size; ++x)
		{
			sum += arr[x][y];
		}
		if (sum!=_magic)
		{
			return 0;
		}

	}
	return 1;
}

int Is_Magic_Square_Column(int arr[size][size], int _magic)
{
	int sum = 0 ;
	int x,y;
	for (x = 0; x < size; ++x)
	{
		sum =0;
		for (y = 0; y < size; ++y)
		{
			sum += arr[x][y];
		}
		if (sum!=_magic)
		{
			return 0;
		}

	}
	return 1;
}

int Is_Magic_Square_Diagonals(int arr[size][size], int _magic)
{
	int sum = 0 ;
	int x,y;
	for (x = 0; x < size; ++x)
	{
		sum += arr[x][x];
	}
	if (sum!=_magic)
	{
		return 0;
	}
	sum =0;
	x=2;
	for (y = 0; y < size; ++y)
	{
		sum += arr[y][x];
		--x;
	}
	if (sum!=_magic)
	{
		return 0;
	}

	return 1;
}






int main(int argc, char const *argv[])
{

	int arr[size][size]={{4,3,8},{9,5,1},{2,7,6}};

	int magicNum;
	magicNum = magic (arr)	;

	if (Is_Magic_Square_Row(arr,magicNum) & Is_Magic_Square_Column(arr,magicNum) & Is_Magic_Square_Diagonals(arr,magicNum))
	{
		printf("Is Magic Square \n" );
	}
	else printf("Is Not Magic Square\n");


	return 0;
}